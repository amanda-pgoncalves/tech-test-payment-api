using PottencialTechTest.Business.Models;

namespace PottencialTechTest.Data.Context
{
    public class DbContext
    {
        public List<Venda> Vendas;
        public List<Vendedor> Vendedores;
        
        public DbContext()
        {
            Vendas = new List<Venda>();

            Vendedores = new List<Vendedor>();
        }
    }
}