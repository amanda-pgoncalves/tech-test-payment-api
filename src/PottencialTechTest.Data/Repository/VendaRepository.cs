using PottencialTechTest.Business.Interfaces;
using PottencialTechTest.Business.Models;
using PottencialTechTest.Business.Models.Enums;
using PottencialTechTest.Data.Context;

namespace PottencialTechTest.Data.Repository
{
    public class VendaRepository : IVendaRepository
    {
        protected readonly DbContext _dbContext;

        public VendaRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public void CadastrarVenda(Venda venda)
        {
            _dbContext.Vendas.Add(venda);
        }

        public Venda BuscarVendaPorId(int id)
        {
            return _dbContext.Vendas.Where(v => v.Id == id).FirstOrDefault();
        }

        public void AtualizarStatusVenda(int id, StatusVenda statusVenda)
        {
            _dbContext.Vendas.Where(v => v.Id == id).FirstOrDefault().Status = statusVenda;
        }
    }
}