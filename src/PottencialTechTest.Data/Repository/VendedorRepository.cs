using PottencialTechTest.Business.Interfaces;
using PottencialTechTest.Business.Models;
using PottencialTechTest.Data.Context;

namespace PottencialTechTest.Data.Repository
{
    public class VendedorRepository : IVendedorRepository
    {
        protected readonly DbContext _dbContext;

        public VendedorRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void CadastrarVendedor(Vendedor vendedor)
        {
            _dbContext.Vendedores.Add(vendedor);
        }

        public Vendedor BuscarVendedorPorId(int id)
        {
            return _dbContext.Vendedores.Where(v => v.Id == id).FirstOrDefault();
        }
    }
}