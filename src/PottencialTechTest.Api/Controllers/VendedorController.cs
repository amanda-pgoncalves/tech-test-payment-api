using Microsoft.AspNetCore.Mvc;
using PottencialTechTest.Business.Interfaces;
using PottencialTechTest.Business.Models;
using System.ComponentModel.DataAnnotations;

namespace PottencialTechTest.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendedorController
    {
        private readonly IVendedorService _vendedorService;

        public VendedorController(IVendedorService vendedorService)
        {
            _vendedorService = vendedorService;
        }

        [HttpPost]
        public void CadastrarVendedor([Required] int id, [Required] string cpf, [Required] string nome, [Required] string email, [Required] string telefone)
        {
            Vendedor vendedor = new Vendedor();
            vendedor.Id = id;
            vendedor.Cpf = cpf;
            vendedor.Nome = nome;
            vendedor.Email = email;
            vendedor.Telefone = telefone;

            _vendedorService.CadastrarVendedor(vendedor);
        }

        [HttpGet]
        public Vendedor BuscarVendedorPorId([Required] int id)
        {
            return _vendedorService.BuscarVendedorPorId(id);
        }
    }
}