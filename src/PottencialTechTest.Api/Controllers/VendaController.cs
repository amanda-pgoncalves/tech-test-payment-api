using Microsoft.AspNetCore.Mvc;
using PottencialTechTest.Business.Interfaces;
using PottencialTechTest.Business.Models;
using PottencialTechTest.Business.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace PottencialTechTest.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendaController
    {
        private readonly IVendedorService _vendedorService;
        private readonly IVendaService _vendaService;

        public VendaController(IVendaService vendaService, IVendedorService vendedorService)
        {
            _vendaService = vendaService;
            _vendedorService = vendedorService;
        }

        [HttpPost]
        public void CadastrarVenda([Required] int id, [Required] int idVendedor, [Required] DateTime dataVenda, [Required] string identificador, [Required] List<Item> itens)
        {
            Venda venda = new Venda();
            venda.Id = id;
            venda.Vendedor = _vendedorService.BuscarVendedorPorId(idVendedor);
            venda.DataVenda = dataVenda;
            venda.Identificador = identificador;
            venda.Itens = itens;
            venda.Status = StatusVenda.AguardandoPagamento;

            _vendaService.CadastrarVenda(venda);
        }

        [HttpGet]
        public Venda BuscarVendaPorId([Required] int id)
        {
            return _vendaService.BuscarVendaPorId(id);
        }

        [HttpPut]
        public void AtualizarStatusVenda([Required] int id, [Required] StatusVenda statusVenda)
        {
            _vendaService.AtualizarStatusVenda(id, statusVenda);
        }

    }
}