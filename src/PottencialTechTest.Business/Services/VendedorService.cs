using PottencialTechTest.Business.Interfaces;
using PottencialTechTest.Business.Models;

namespace PottencialTechTest.Business.Services
{
    public class VendedorService : IVendedorService
    {
        private readonly IVendedorRepository _vendedorRepository;
        public VendedorService(IVendedorRepository vendedorRepository)
        {
            _vendedorRepository = vendedorRepository;
        }

        public void CadastrarVendedor(Vendedor vendedor)
        {
            if (vendedor.Id <= 0)
            {
                throw new Exception("� necess�rio que o vendedor tenha um id cadastrado");
            }
            if (string.IsNullOrEmpty(vendedor.Cpf))
            {
                throw new Exception("� necess�rio que o vendedor tenha um cpf cadastrado");
            }

            if (string.IsNullOrEmpty(vendedor.Nome))
            {
                throw new Exception("� necess�rio que o vendedor tenha um nome cadastrado");
            }

            if (string.IsNullOrEmpty(vendedor.Email))
            {
                throw new Exception("� necess�rio que o vendedor tenha um e-mail cadastrado");
            }

            if (string.IsNullOrEmpty(vendedor.Telefone))
            {
                throw new Exception("� necess�rio que o vendedor tenha um telefone cadastrado");
            }
            _vendedorRepository.CadastrarVendedor(vendedor);
        }
        public Vendedor BuscarVendedorPorId(int id)
        {
            return _vendedorRepository.BuscarVendedorPorId(id);
        }
    }
}