using PottencialTechTest.Business.Interfaces;
using PottencialTechTest.Business.Models;
using PottencialTechTest.Business.Models.Enums;

namespace PottencialTechTest.Business.Services
{
    public class VendaService : IVendaService
    {
        private readonly IVendaRepository _vendaRepository;

        public VendaService(IVendaRepository vendaRepository)
        {
            _vendaRepository = vendaRepository;
        }

        public void AtualizarStatusVenda(int id, StatusVenda statusVenda)
        {
            var venda = BuscarVendaPorId(id);
            if (statusVenda == StatusVenda.PagamentoAprovado && venda.Status != StatusVenda.AguardandoPagamento)
            {
                throw new Exception("Não é possível atualizar o status dessa venda");
            }

            if (statusVenda == StatusVenda.Cancelada && venda.Status != StatusVenda.AguardandoPagamento && venda.Status != StatusVenda.PagamentoAprovado)
            {
                throw new Exception("Não é possível atualizar o status dessa venda");
            }

            if (statusVenda == StatusVenda.EnviadoParaTransportadora && venda.Status != StatusVenda.PagamentoAprovado)
            {
                throw new Exception("Não é possível atualizar o status dessa venda");
            }

            if (statusVenda == StatusVenda.Entregue && venda.Status != StatusVenda.EnviadoParaTransportadora)
            {
                throw new Exception("Não é possível atualizar o status dessa venda");
            }

            if (venda.Status == StatusVenda.Entregue)
            {
                throw new Exception("Não é possível atualizar o status de venda que já foi entregue");
            }

            if (statusVenda == StatusVenda.AguardandoPagamento)
            {
                throw new Exception("Não é possível voltar status da venda para Aguardando Pagamento");
            }

            _vendaRepository.AtualizarStatusVenda(id, statusVenda);
        }

        public Venda BuscarVendaPorId(int id)
        {
            return _vendaRepository.BuscarVendaPorId(id);
        }

        public void CadastrarVenda(Venda venda)
        {
            if (venda.Itens == null || venda.Itens.Count() == 0 || venda.Itens.Where(i => i.Id == 0).Any())
            {
                throw new Exception("A inclusão da venda deve possui pelo menos 1 item");
            }

            if (venda.Status != StatusVenda.AguardandoPagamento)
            {
                throw new Exception("O status inicial da venda deve ser Aguardando Pagamento");
            }

            if (venda.Vendedor.Id <= 0)
            {
                throw new Exception("É necessário que o vendedor tenha um id cadastrado");
            }

            if (string.IsNullOrEmpty(venda.Vendedor.Cpf))
            {
                throw new Exception("É necessário que o vendedor tenha um cpf cadastrado");
            }

            if (string.IsNullOrEmpty(venda.Vendedor.Nome))
            {
                throw new Exception("É necessário que o vendedor tenha um nome cadastrado");
            }

            if (string.IsNullOrEmpty(venda.Vendedor.Email))
            {
                throw new Exception("É necessário que o vendedor tenha um e-mail cadastrado");
            }

            if (string.IsNullOrEmpty(venda.Vendedor.Telefone))
            {
                throw new Exception("É necessário que o vendedor tenha um telefone cadastrado");
            }

            _vendaRepository.CadastrarVenda(venda);

        }
    }
}