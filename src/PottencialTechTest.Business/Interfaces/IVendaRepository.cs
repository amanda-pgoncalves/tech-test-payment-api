using PottencialTechTest.Business.Models;
using PottencialTechTest.Business.Models.Enums;

namespace PottencialTechTest.Business.Interfaces
{
    public interface IVendaRepository
    {
        public void CadastrarVenda(Venda venda);

        public Venda BuscarVendaPorId(int id);

        public void AtualizarStatusVenda(int id, StatusVenda statusVenda);
    }
}