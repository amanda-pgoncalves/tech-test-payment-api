using PottencialTechTest.Business.Models;

namespace PottencialTechTest.Business.Interfaces
{
    public interface IVendedorService
    {
        public void CadastrarVendedor(Vendedor vendedor);

        public Vendedor BuscarVendedorPorId(int id);
    }
}