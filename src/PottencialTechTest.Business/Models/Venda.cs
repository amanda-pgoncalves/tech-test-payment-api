using PottencialTechTest.Business.Models.Enums;

namespace PottencialTechTest.Business.Models
{
    public class Venda
    {
        public int Id { get; set; }

        public Vendedor Vendedor { get; set; }

        public DateTime DataVenda { get; set; }

        public string Identificador { get; set; }

        public List<Item> Itens { get; set; }

        public StatusVenda Status { get; set; }
    }
}