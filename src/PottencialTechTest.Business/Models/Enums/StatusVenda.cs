namespace PottencialTechTest.Business.Models.Enums
{
    public enum StatusVenda
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada

    }
}